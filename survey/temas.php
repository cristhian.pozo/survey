<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

//Get Input data from query string

$page = filter_input(INPUT_GET, 'page');
//Per page limit for pagination.
$pagelimit = 20;

$db = getDbInstance();
if (!$page) {
    $page = 1;
}


$db = getDbInstance();
// select the columns
$select = array( 'f_name');

//Set pagination limit
$db->pageLimit = $pagelimit;

//Get result of the query.
$customers = $db->arraybuilder()->paginate("themes", $page, $select);
$total_pages = $db->totalPages;

// get columns for order filter
foreach ($customers as $value) {
    foreach ($value as $col_name => $col_value) {
        $filter_options[$col_name] = $col_name;
    }
    //execute only once
    break;
}
include_once 'includes/header.php';
?>

<!--Main container start-->
<div id="page-wrapper">
    <div class="row">

        <div class="col-lg-6">
            <h1 class="page-header">Temas</h1>
        </div>
         <div class="col-lg-6" style=""> 
            <div class="page-action-links text-right">
	           
            </div>
         </div> 
    </div>
        <?php include('./includes/flash_messages.php') ?>
    <!--    Begin filter section-->
    

    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $row) : ?>
                <tr>
	                <td>
                        <a href="add_questionary.php?name=<?php echo htmlspecialchars($row['f_name']); ?>">
                            <?php echo htmlspecialchars($row['f_name']); ?>
                        </a>
                    </td>
				</tr>	
            <?php endforeach; ?>      
        </tbody>
    </table>


   
<!--    Pagination links-->
    <div class="text-center">

        <?php
        if (!empty($_GET)) {
            //we must unset $_GET[page] if previously built by http_build_query function
            unset($_GET['page']);
            //to keep the query sting parameters intact while navigating to next/prev page,
            $http_query = "?" . http_build_query($_GET);
        } else {
            $http_query = "?";
        }
        //Show pagination links
        if ($total_pages > 1) {
            echo '<ul class="pagination text-center">';
            for ($i = 1; $i <= $total_pages; $i++) {
                ($page == $i) ? $li_class = ' class="active"' : $li_class = "";
                echo '<li' . $li_class . '><a href="customers.php' . $http_query . '&page=' . $i . '">' . $i . '</a></li>';
            }
            echo '</ul></div>';
        }
        ?>
    </div>
    <!--    Pagination links end-->

</div>
<!--Main container end-->




