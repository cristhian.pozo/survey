<?php 
require_once './config/config.php';
session_start();
if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
    $username = filter_input(INPUT_POST, 'name');
    $last = filter_input(INPUT_POST, 'last');
    $email = filter_input(INPUT_POST, 'email');
    $last=  $last;
   	
    //Get DB instance. function is defined in config.php
    $db = getDbInstance();

    $db->where ("name", $username);
    $db->where ("last", $last);
    $db->where ("email", $last);
    $row = $db->get('user_accounts');
     
    if ($db->count >= 1) {
        $_SESSION['user_logged_in'] = false;
        $_SESSION['only_user'] = TRUE;
       	if($email)
       	{
       		setcookie('name',$username , time() + (86400 * 90), "/");
       		setcookie('last',$last , time() + (86400 * 90), "/");
       	}
        header('Location:index.php');
        exit;
    } else {
        $_SESSION['login_failure'] = "Invalid user name or password";
        header('Location:login.php');
        exit;
    }
  
}